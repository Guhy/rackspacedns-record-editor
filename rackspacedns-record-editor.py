#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Rackspace DNS record editor.
   This Python script allows you to add/remove/update/list DNS records from your Rackspace account.
   Simply pass apikey, username, domain and record which you would like to work with.
"""

from collections import namedtuple
from ipaddress import ip_address
import sys
import argparse
import json
import requests

global domain_id

def authorize_in_rackspace(base_username, base_apikey):
    auth = namedtuple('auth',['auth_token', 'api_endpoint'])

    headers = {'Content-type': 'application/json'}
    rackspace_url = "https://identity.api.rackspacecloud.com/v2.0/tokens"
    request_data = json.dumps({
        "auth": {
            "RAX-KSKEY:apiKeyCredentials": {
                "username":base_username,
                "apiKey":base_apikey
             }
          }
    })
    response = requests.post(rackspace_url, data = request_data, headers = headers)
    response.raise_for_status()
    data = response.json()
    auth_token = data["access"]["token"]["id"]

    for service in data["access"]["serviceCatalog"]:
        if service["name"] == "cloudDNS":
            api_endpoint = service["endpoints"][0]["publicURL"]

    return auth(auth_token, api_endpoint)

def get_domain_id(auth_token, base_url, domain_name):
    headers = {'X-Auth-Token':auth_token}
    url = f'{base_url}/domains/'
    response = requests.get(url, headers = headers)
    response.raise_for_status()

    for domain in response.json()["domains"]:
        if domain["name"] == domain_name:
            return domain["id"]
    sys.exit(f'Domain {domain_name} was not found')

def add_dns_record(args):
    headers = {'Content-type': 'application/json', 'X-Auth-Token': rackspace_auth.auth_token}
    url = f'{rackspace_auth.api_endpoint}/domains/{domain_id}/records'
    request_data = json.dumps({
        "records": [{
            "name" : args.record,
            "type" : args.type,
            "data" : str(args.data),
            "ttl" : args.ttl
        }]
    })
    response = requests.post(url, data = request_data, headers = headers)
    print(f'Response status code: {response.status_code}')
    response.raise_for_status()
    print('Added new record:')
    show_dns_record(args)

def delete_dns_record(args):
    record_details = get_dns_record(args)

    print('Record details before deletion')
    show_dns_record(args)
    headers = {'X-Auth-Token': rackspace_auth.auth_token}
    url = f'{rackspace_auth.api_endpoint}/domains/{domain_id}/records/{record_details.id}'
    response = requests.delete(url, headers = headers)
    response.raise_for_status()
    print(f'Deleted record {record_details.id}')

def update_dns_record(args):
    record_details = get_dns_record(args)
    record_ttl = record_details.ttl

    if args.ttl is not None:
        record_ttl = args.ttl

    print('Record details before update:')
    show_dns_record(args)
    headers = {'Content-type': 'application/json', 'X-Auth-Token': rackspace_auth.auth_token}
    request_data = json.dumps({
        "records": [{
            "name" : args.record,
            "id" : record_details.id,
            "data" : str(args.data),
            "ttl" : record_ttl
        }]
    })
    url = f'{rackspace_auth.api_endpoint}/domains/{domain_id}/records/'
    response = requests.put(url, data = request_data, headers = headers)
    response.raise_for_status() 
    print(response, response.content)

    print('Record details after update:')
    show_dns_record(args)

def get_dns_record(args):
    record_details = namedtuple('record_details', ['id', 'name', 'data', 'ttl'])

    url = f'{rackspace_auth.api_endpoint}/domains/{domain_id}'
    headers = {'X-Auth-Token': rackspace_auth.auth_token}
    params = {'showRecords':'true', 'showSubdomain':'false'}

    response = requests.get(url, headers = headers, params = params)
    response.raise_for_status()

    for record in response.json()["recordsList"]["records"]:
        if record["name"] == args.record:
            return record_details(record["id"], record["name"], record["data"], record["ttl"])
    sys.exit(f'Record {args.record} was not found')

def show_dns_record(args):
    record_details = get_dns_record(args)
    return print(f'ID: {record_details.id} \n\rName: {record_details.name} \n\rData: {record_details.data} \n\rTTL: {record_details.ttl}')

# argument parser
parent_parser = argparse.ArgumentParser(
    description='Add/remove/update DNS record in your Rackspace account. Simply pass you apikey, DNS record and maybe IP address',
    epilog='kuniec'
)
parent_parser.add_argument('--apikey', help='Api key for you rackspace account', required=True)
parent_parser.add_argument('--username', help='Username key for you rackspace account', required=True)

subparser = parent_parser.add_subparsers(help='available commands')

# parser for add command
parser_add_dns_record = subparser.add_parser('add', help='Add new DNS record')
parser_add_dns_record.add_argument('--domain', help='Rackspace domain name', required=True)
parser_add_dns_record.add_argument('--record', help='DNS record to add/remove/update/show', required=True)
parser_add_dns_record.add_argument('--data', help='The data field is required for PTR and TXT records only. For PTR records, the data field must be a valid IPv4 or IPv6 IP address.', type=ip_address, required=True)
parser_add_dns_record.add_argument('--type', help='Type of DNS record to add', choices=['CNAME', 'MX', 'TXT', 'A'], required=True)
parser_add_dns_record.add_argument('--ttl', help='[Optional] TTL for new DNS record | Default value 3600', type=int, default=3600)
parser_add_dns_record.set_defaults(func=add_dns_record)

# parser for remove command
parser_remove_dns_record = subparser.add_parser('remove', help='Remove existing DNS record')
parser_remove_dns_record.add_argument('--domain', help='Rakcspace domain name', required=True)
parser_remove_dns_record.add_argument('--record', help='DNS record to remove')
parser_remove_dns_record.set_defaults(func=delete_dns_record)

# parser for update command
parser_update_dns_record = subparser.add_parser('update', help='Update existing DNS record')
parser_update_dns_record.add_argument('--domain', help='Rackspace domain name', required=True)
parser_update_dns_record.add_argument('--record', help='DNS record to update', required=True)
parser_update_dns_record.add_argument('--data', help='The data field is required for PTR and TXT records only. For PTR records, the data field must be a valid IPv4 or IPv6 IP address.', required=True, type=ip_address)
parser_update_dns_record.add_argument('--ttl', help='[Optional] TTL for new DNS record | No default value', type=int, default=None)
parser_update_dns_record.set_defaults(func=update_dns_record)

# parser for show command
parser_show_dns_record = subparser.add_parser('show', help='show DNS entries for domain')
parser_show_dns_record.add_argument('--domain', help='Rackspace domain name', required=True)
parser_show_dns_record.add_argument('--record', help='DNS record to show', required=True)
parser_show_dns_record.set_defaults(func=show_dns_record)

if __name__ == '__main__':
    args = parent_parser.parse_args(sys.argv[1:])
 
    rackspace_auth = authorize_in_rackspace(args.username,args.apikey)
    domain_id = get_domain_id(rackspace_auth.auth_token, rackspace_auth.api_endpoint, args.domain)
    print(f'Domain ID for {args.domain} is {domain_id}')

    args.func(args)
